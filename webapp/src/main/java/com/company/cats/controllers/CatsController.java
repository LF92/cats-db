package com.company.cats.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.company.cats.domain.Cat;
import com.company.cats.dto.CatDTO;
import com.company.cats.service.CatServiceI;
import com.company.toys.service.ToyServiceI;

@Controller
@RequestMapping("cat")
public class CatsController {

	/**
	 * For non-service solutions:
	 * 
	 * @Autowired @Qualifier("CatDaoRepository") private CatDaoRepository
	 *            catService;
	 */

	@Autowired
	@Qualifier("CatService")
	private CatServiceI catService;

	@Autowired
	@Qualifier("ToyService")
	private ToyServiceI toyService;

	@RequestMapping(method = RequestMethod.GET, value = { "/", "cat", "*", "/list" })
	public String catsList(Model model) {
		model.addAttribute("cat", catService.findAll());
		return "list";
	}

	@RequestMapping(method = RequestMethod.POST, value = "add")
	public String addCat(HttpServletRequest request, @ModelAttribute("catDto") @Valid CatDTO catDto,
			BindingResult result) {
		if (request.getMethod().equalsIgnoreCase("post") && !result.hasErrors()) {
			Cat cat = new Cat();
			cat.setId(null);
			cat.setName(catDto.getName());
			cat.setBirthDate(catDto.getBirthDate());
			cat.setWeight(catDto.getWeight());
			cat.setOwnerName(catDto.getOwnerName());
			cat.setToyList(catDto.getToyList());
			catService.save(cat);

			return "redirect:/cat/list";
		}
		return "add";
	}

	@RequestMapping(method = RequestMethod.GET, value = "id/{id}")
	public String catDetails(@PathVariable("id") Long id, Model model) {
		model.addAttribute("cat", catService.findById(id));
		model.addAttribute("toys", toyService.findByCatOwner(catService.findById(id)));
		return "details";
	}

	// @Secured("ROLE_USER")
	@RequestMapping(method = RequestMethod.POST, value = "id/{id}/delete")
	public String catDelete(@PathVariable("id") Long id) {

		if (isLogged()) {
			toyService.deleteAll(catService.findById(id));
			catService.delete(catService.findById(id));
			return "redirect:/cat/list";
		} else
			return "redirect:/login";
	}
	
	private Boolean isLogged() {
		return !(SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof String);
	}
}
