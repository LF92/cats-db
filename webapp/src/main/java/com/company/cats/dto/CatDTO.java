package com.company.cats.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

import com.company.toys.domain.Toy;

import java.util.Date;
import java.util.List;

public class CatDTO {

	@NotNull
	private Long id = 0L;

	@NotBlank(message = "The \"name\" field can not be empty")
	private String name;

	// @Pattern(regexp = "[0-3]?[0-9]\\/[0-1]?[0-9]\\/[1-2][0-9]{3}")
	private Date birthDate;

	@NotNull
	private Float weight;

	@NotBlank
	private String ownerName;

	private List<Toy> toyList;

	public CatDTO() {
	}

	public CatDTO(@NotNull Long id, @NotBlank(message = "The \"name\" field can not be empty") String name,
			Date birthDate, @NotNull Float weight, @NotBlank String ownerName, List<Toy> toyList) {
		this.id = id;
		this.name = name;
		this.birthDate = birthDate;
		this.weight = weight;
		this.ownerName = ownerName;
		this.toyList = toyList;
	}

	public List<Toy> getToyList() {
		return toyList;
	}

	public void setToyList(List<Toy> toyList) {
		this.toyList = toyList;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
}
