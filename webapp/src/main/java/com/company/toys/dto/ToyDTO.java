package com.company.toys.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

//import com.company.cats.domain.Cat;

public class ToyDTO {

	@NotNull
	private Long id = 15L;

	@NotBlank
	private String name;

//	@NotNull
//	private Cat catOwner;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

//	public Cat getCatOwner() {
//		return catOwner;
//	}
//
//	public void setCatOwner(Cat catOwner) {
//		this.catOwner = catOwner;
//	}
}
