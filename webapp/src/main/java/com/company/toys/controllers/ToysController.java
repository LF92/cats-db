package com.company.toys.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.company.cats.service.CatServiceI;
import com.company.toys.domain.Toy;
import com.company.toys.dto.ToyDTO;
import com.company.toys.service.ToyServiceI;

@Controller
@RequestMapping("toy")
public class ToysController {

	@Autowired
	@Qualifier("CatService")
	private CatServiceI catService;

	@Autowired
	@Qualifier("ToyService")
	public ToyServiceI toyService;

	@RequestMapping(method = RequestMethod.GET, value = "/cat/id/{id}/toylist/")
	public String toysList(@PathVariable("id") Long id, Model model) {
		model.addAttribute("toy", toyService.findByCatOwner(catService.findById(id)));
		model.addAttribute("cat", catService.findById(id));
		return "toys/toyList";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cat/id/{id}/toylist/add")
	public String addCat(HttpServletRequest request, @ModelAttribute("toyDto") @Valid ToyDTO toyDto,
			BindingResult result, @PathVariable("id") Long id) {
		if (request.getMethod().equalsIgnoreCase("post") && !result.hasErrors()) {
			Toy toy = new Toy();
			toy.setId(null);
			toy.setName(toyDto.getName());
			toy.setCatOwner(catService.findById(id));
			toyService.save(toy);

			return "redirect:/toy/cat/id/{id}/toylist/";
		}
		return "toys/add";
	}
}
