package com.company.users.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.company.users.UserDaoRepository;
import com.company.users.domain.User;
import com.company.users.dto.UserDTO;

@Controller
//@RequestMapping("user")
public class UserController {

	@Autowired
	@Qualifier("UserDaoRepository")
	private UserDaoRepository userRepo;

	@RequestMapping(value = "/register")
	public String showRegistration(HttpServletRequest request, @ModelAttribute("userDto") @Valid UserDTO userDto,
			BindingResult result) {

		if (request.getMethod().equalsIgnoreCase("post") && !result.hasErrors()) {
			User user = new User();
			user.setId(null);
			user.setUsername(userDto.getUsername());
			user.setPassword(userDto.getPassword());
			user.setEnabled(userDto.getEnabled());
			userRepo.save(user);

			return "redirect:/cat/list";
		}
		return "users/register";
	}
}
