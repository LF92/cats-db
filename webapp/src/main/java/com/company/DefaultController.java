package com.company;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultController {

	@RequestMapping("/")
	public String redirectToCatsList() {
		return "redirect:/cat";
	}

	@RequestMapping("/login")
	public String redirectToLogin() {
		return "users/login";
	}
}
