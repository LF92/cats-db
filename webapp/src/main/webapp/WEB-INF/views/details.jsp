<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

	<jsp:include page="./header.jsp" />

	<title>Details</title>
	
	<body>
	
		<jsp:include page="./navbar.jsp" />		
	
		<div class="container">
		
		<h2>Details:</h2>
		
			<table style="text-align: center;" class="table table-striped table-bordered">
			
				<tr>
					<th style="width: 25%">Id</th>
					<td colspan="2">${cat.getId()}</td>
				</tr>
				
				<tr>
					<th>Name</th>
					<td colspan="2">${cat.getName()}</td>
				</tr>
				
				<tr>
					<th>Birth date</th>
					<td colspan="2"><fmt:formatDate pattern="dd/MM/yyyy" value="${cat.getBirthDate()}" /></td>
				</tr>
				
				<tr>
					<th>Weight</th>
					<td colspan="2"><fmt:formatNumber type="number" minFractionDigits="2"
						maxFractionDigits="2" value="${cat.getWeight()}" /> kg</td>
				</tr>
				
				<tr>
					<th>Owner name</th>
					<td colspan="2">${cat.getOwnerName()}</td>
				</tr>
				
				<tr>
					<th>Number of toys:</th>
					<td colspan="2">${toys.size()}</td>
				</tr>
				
				<tr>
					<th>Toys list:</th>
					<td>
						<c:forEach var="toy" items="${toys}">
						<a href="<c:url value="/toy/cat/id/${toy.getId()}/toys" />">${toy.getName()}, </a>
						</c:forEach>

					</td>
					<td>
						<form method="GET" id="toyDetails" action="<c:url value="/toy/cat/id/${cat.getId()}/toylist/" />">
							<button type="submit" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Edit</button>
						</form>
					</td>
				</tr>
			
			</table>
			
			<button onclick="goBack()" class="btn btn-primary btn-md"><i class="fa fa-arrow-left"></i> Back</button>
			
		</div>
		
		<jsp:include page="./footer.jsp" />
		
	</body>
</html>