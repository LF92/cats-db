<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

	<jsp:include page="./header.jsp" />

	<title>Cats list</title>
	
	<body>
	
		<jsp:include page="./navbar.jsp" />		
		
		<c:choose>
			<c:when test="${not empty cat}">
				<div class="container">
				
					<table class="table">
						<thead class="bg-info text-white font-weight-bold">
							<tr>
								<td style="width: 8%;">Cat ID</td>
								<td>Name</td>
								<td>Birth date</td>
								<td>Weight</td>
								<td>Owner name</td>
								<td colspan="2" style="text-align: center;">Operation</td>
							</tr>
						</thead>
						<c:forEach var="cat" items="${cat}">
							<tr>
								<td style="width: 8%;">${cat.getId()}</td>
								<td><a href="<c:url value="id/${cat.getId()}" />">${cat.getName()}</a></td>
								<td><fmt:formatDate pattern="dd/MM/yyyy" value="${cat.getBirthDate()}" /></td>
								<td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${cat.getWeight()}" /> kg</td>
								<td>${cat.getOwnerName()}</td>
							
								<td colspan="2">
									<div class="container" >
										<div class="row justify-content-start">
											<div class="col-3">
												<form action="<c:url value="id/${cat.getId()}/edit" />" method="POST">
													<button type="submit" class="btn btn-secondary"><i class="fa fa-pencil-square-o"></i> Edit</button>
												</form> 
											</div>
											<div class="col-3">
												<form action="<c:url value="id/${cat.getId()}/delete" />" method="POST">
													<button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</button>
												</form> 
											</div>
										</div>
									</div>
								</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</c:when>                    
			<c:otherwise>        
				<div class="alert alert-info">No cat found matching your search criteria</div>
			</c:otherwise>
		</c:choose> 

		<br /><br />
		
		<div class="container">
			<form  method="POST" action ="add"><button type="submit" class="btn btn-success btn-md"><i class="fa fa-plus"></i> New cat</button></form>	
		</div>
		
		
		<jsp:include page="./footer.jsp" />
		
	</body>
</html>
