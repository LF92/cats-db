<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	<jsp:include page="../header.jsp" />
	
	<title>Add new</title>
	
	<body>
	
		<jsp:include page="../navbar.jsp" />
	
		<div class="container">
		
		<h2>Add new:</h2>
		
			<form:form method="POST" modelAttribute="toyDto">
				<div class="form-group col-xs-4">
					<label for="name" class="control-label col-xs-4">Toy name</label>
					<input type="text" name="name" id="name" class="form-control" value="${toy.getName()}" required="true" />
				</div>
				<button onclick="goBack()" class="btn btn-primary btn-md"><i class="fa fa-arrow-left"></i> Back</button>
				<button type="submit" class="btn btn-primary btn-md"><i class="fa fa-check"></i> Accept</button>
			</form:form>
		
		</div>

		<jsp:include page="../footer.jsp" />
	
	</body>
	
</html>