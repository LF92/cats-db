<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

	<jsp:include page="../header.jsp" />

	<title>Editing toy list</title>
	
	<body>		
	
		<jsp:include page="../navbar.jsp" />
	
		<c:choose>
			<c:when test="${not empty toy}">
				<div class="container">
				
				<h2>List of toys: ${toy[0].getCatOwner().getOwnerName()}</h2>
				
					<table  class="table">
						<thead class="table-dark">
							<tr >
								<td>Toy ID</td>
								<td>Name</td>
								<td>Owner id</td>
							</tr>
						</thead>
						<c:forEach var="toy" items="${toy}">
							<tr>
								<td>${toy.getId()}</td>
								<td>${toy.getName()}</td>
								<td>${toy.getCatOwner().getId()}</td>
							</tr>
						</c:forEach>
						
					</table>
				</div>
			</c:when>                    
			<c:otherwise>        
				<div class="container alert alert-info">No cat found matching your search criteria</div>
			</c:otherwise>
		</c:choose> 
		
		<div class="container">
			<form id="formNewToy" method="POST" action="<c:url value="/toy/cat/id/${cat.getId()}/toylist/add" />"></form>
	
			<button onclick="goBack()" class="btn btn-primary  btn-md"><i class="fa fa-arrow-left"></i> Back</button>
			<button type="submit" form="formNewToy" class="btn btn-primary btn-md"><i class="fa fa-plus"></i> Add new toy</button>
		</div>
			
		<jsp:include page="../footer.jsp" />

	</body>
</html>
