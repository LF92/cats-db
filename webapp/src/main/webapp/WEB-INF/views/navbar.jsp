<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand" href="#">CatsDB</a>
	
	<button class="navbar-toggler" type="button"
			data-toggle="collapse" data-target="#navbarNav"
			aria-controls="navbarNav" aria-expanded="false"
			aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="http://localhost:8080/cats-webapp/cat/list">Home <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Features</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Pricing</a>
			</li>
			<li class="nav-item">
				<a class="nav-link disabled" href="#">Disabled</a>
			</li>
		</ul>
		
		<c:set var = "islogged" scope = "session" value = "0"/>
		
		<sec:authorize access="hasRole('ROLE_USER')">
			<c:set var = "islogged" scope = "session" value = "1"/>
		</sec:authorize>
		
		<c:if test = "${islogged == 0}">
			<ul class="navbar-nav ">
				<li class="nav-item">
					<form class="form-inline" method="GET" action="<c:url value="/register" />">
				      <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Sign up</button>
				    </form>
			    </li>
			    <li class="nav-item">
					<form class="form-inline mx-3" method="GET" action="<c:url value="/login" />">
				      <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Sign in</button>
				    </form>
			    </li>
			</ul>
      	</c:if>
		
		
		<c:if test = "${islogged == 1}">
			<ul class="navbar-nav">
				<li class="nav-item">
					<p class="text-light my-auto mx-5 font-weight-bold">You are logged in as  <sec:authentication property="principal.username" /></p>
				</li>
				<li class="nav-item">
					<form class="form-inline" method="GET" action="<c:url value="/logout" />">
				      <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Logout</button>
				    </form>
			    </li>
			</ul>
      	</c:if>
      	
	</div>
</nav>
<br /><br /><br />