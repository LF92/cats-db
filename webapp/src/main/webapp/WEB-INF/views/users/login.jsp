<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	<jsp:include page="../header.jsp" />
	
	<title>Login</title>

	<body>
		<jsp:include page="../navbar.jsp" />
		
		<div class="container">
			<h1>Login:</h1>
			
			<form:form method="post" action="login">
				<div class="form-group col-xs-4">
					<input type="text" name="username" placeholder="Enter username" />
					<input type="password" name="password" placeholder="Enter password" />
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<button type="submit" class="btn btn-primary btn-md"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</button>
				</div>
			</form:form>
		</div>
		<jsp:include page="../footer.jsp" />
	</body>
</html>