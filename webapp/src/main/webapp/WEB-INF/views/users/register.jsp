<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	<jsp:include page="../header.jsp" />
	
	<title>Register</title>

	<body>
	
		<jsp:include page="../navbar.jsp" />
	
		<div class="container">
		
		<h2>Register:</h2>
		
			<form:form method="POST" modelAttribute="userDto">
				<div class="form-group col-xs-4">
					<label for="username" class="control-label col-xs-4">Username</label>
					<input type="text" name="username" id="username" class="form-control" required="true" />
					
					<label for="password" class="control-label col-xs-4">Password</label>
					<input type="password" name="password" id="password" class="form-control" required="true" />								
					
				</div>
				<button onclick="goBack()" class="btn btn-primary btn-md"><i class="fa fa-arrow-left"></i> Back</button>
				<button type="submit" class="btn btn-primary btn-md"><i class="fa fa-check"></i> Accept</button>
			</form:form>
		
		</div>
		
		<jsp:include page="../footer.jsp" />
		
	</body>
</html>