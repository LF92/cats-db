<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

	<jsp:include page="./header.jsp" />
	
	<title>Add new</title>

	<body>
		
		<jsp:include page="./navbar.jsp" />
	
		<div class="container">
		
		<h2>Add new:</h2>
		
			<form:form method="POST" modelAttribute="catDto">
				<div class="form-group col-xs-4">
					<label for="name" class="control-label col-xs-4">Name</label>
					<input type="text" name="name" id="name" class="form-control" required="true" />
					
					<label for="birthDate" pattern="^\d{2}\/\d{2}\/\d{4}$" maxlength="10" placeholder="dd/MM/yyy" class="control-label col-xs-4">Birth Date</label>
					<input type="text" name="birthDate" id="birthDate" class="form-control" required="true" />
					
					<label for="weight" class="control-label col-xs-4">Weight</label>
					<input type="text" name="weight" id="weight" class="form-control" required="true" />
					
					<label for="ownerName" class="control-label col-xs-4">Owner Name</label>
					<input type="text" name="ownerName" id="ownerName" class="form-control" required="true" />
				</div>
				<button onclick="goBack()" class="btn btn-primary btn-md"><i class="fa fa-arrow-left"></i> Back</button>
				<button type="submit" class="btn btn-primary btn-md"><i class="fa fa-check"></i> Accept</button>
			</form:form>
		
		</div>
		
		<jsp:include page="./footer.jsp" />
		
	</body>
</html>