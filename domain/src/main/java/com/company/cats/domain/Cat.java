package com.company.cats.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.company.toys.domain.Toy;

@Entity
@Table(name = "cats")
public class Cat {

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id = 0L;

	@Column(name = "name")
	private String name;

	@Column(name = "birth_date")
	private Date birthDate;

	@Column(name = "weight")
	private Float weight;

	@Column(name = "owner_name")
	private String ownerName;

	@OneToMany(mappedBy = "catOwner", fetch = FetchType.LAZY)
	private List<Toy> toyList;

	public Cat() {

	}

	public Cat(Long id, String name, Date birthDate, Float weight, String ownerName, List<Toy> toyList) {
		super();
		this.id = id;
		this.name = name;
		this.birthDate = birthDate;
		this.weight = weight;
		this.ownerName = ownerName;
		this.toyList = toyList;
	}

	@Override
	public String toString() {
		return "Cat [id=" + id + ", name=" + name + ", birthDate=" + birthDate + ", weight=" + weight + ", ownerName="
				+ ownerName + ", toyList=" + "toyList" + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public List<Toy> getToyList() {
		return toyList;
	}

	public void setToyList(List<Toy> toyList) {
		this.toyList = toyList;
	}

}
