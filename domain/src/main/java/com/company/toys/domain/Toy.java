package com.company.toys.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import com.company.cats.domain.Cat;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "toys")
public class Toy {

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id = 0L;

	@Column(name = "name")
	private String name;

	@ManyToOne()
	@JoinColumn(name = "owner_id", nullable = false)
	private Cat catOwner;

	public Toy() {
	}

	public Toy(Long id, String name, Cat catOwner) {
		super();
		this.id = id;
		this.name = name;
		this.catOwner = catOwner;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Cat getCatOwner() {
		return catOwner;
	}

	public void setCatOwner(Cat catOwner) {
		this.catOwner = catOwner;
	}

}
