package com.company.cats.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.company.cats.CatDaoRepository;
import com.company.cats.domain.Cat;
import com.company.cats.service.CatServiceI;

@Component
@Qualifier("CatService")
public class CatService implements CatServiceI {

	@Autowired
	@Qualifier("CatDaoRepository")
	private CatDaoRepository catRepository;

	public Cat findById(Long id) {
		return catRepository.findById(id);
	}

	public List<Cat> findAll() {
		return catRepository.findAll();
	}

	public void save(Cat cat) {
		catRepository.save(cat);
	}

	public void delete(Cat cat) {
		catRepository.delete(cat);
	}

	public Long count() {
		return catRepository.count();
	}

	public void delete(Long id) {
		delete(findById(id));		
	}
}
