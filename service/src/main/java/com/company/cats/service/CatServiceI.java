package com.company.cats.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.company.cats.domain.Cat;

@Service
public interface CatServiceI {

	public <T extends Cat> List<T> findAll();

	public Cat findById(Long id);

	public void save(Cat cat);

	public void delete(Cat cat);
	
	public void delete(Long id);

	public Long count();
}
