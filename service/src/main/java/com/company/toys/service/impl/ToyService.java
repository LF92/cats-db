package com.company.toys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.company.cats.domain.Cat;
import com.company.toys.ToyDaoRepository;
import com.company.toys.domain.Toy;
import com.company.toys.service.ToyServiceI;

@Component
@Qualifier("ToyService")
public class ToyService implements ToyServiceI {

	@Autowired
	@Qualifier("ToyDaoRepository")
	public ToyDaoRepository toyRepository;

	@Override
	public <T extends Toy> List<T> findByCatOwner(Cat cat) {
		return toyRepository.findByCatOwner(cat);
	}

	@Override
	public void delete(Toy toy) {
		toyRepository.delete(toy);
	}

	@Override
	public void save(Toy toy) {
		toyRepository.save(toy);
	}

	@Override
	public Long count() {
		return toyRepository.count();
	}

	@Override
	public void deleteAll(Cat cat) {
		for (Toy toy : findByCatOwner(cat))
			delete(toy);
	}

}
