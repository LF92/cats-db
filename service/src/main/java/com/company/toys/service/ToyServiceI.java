package com.company.toys.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.company.cats.domain.Cat;
import com.company.toys.domain.Toy;

@Service
public interface ToyServiceI {
	
	public <T extends Toy> List<T> findByCatOwner(Cat cat);

	public void delete(Toy toy);

	public void save(Toy toy);
	
	public Long count();
	
	public void deleteAll(Cat cat);
}
