package com.company.users;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.company.users.domain.User;

@Repository
@Qualifier("UserDaoRepository")
public interface UserDaoRepository extends org.springframework.data.repository.Repository<User, Long> {
	public User findById(Long id);
	
	public <T extends User> List<T> findAll();

	public void save(User user);

	public void delete(User user);

	public Long count();
}
