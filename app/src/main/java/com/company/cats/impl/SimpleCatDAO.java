package com.company.cats.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.company.cats.CatDaoRepository;
import com.company.cats.domain.Cat;

@Component
@Qualifier("SimpleCatDAO")
public class SimpleCatDAO implements CatDaoRepository {
	private static List<Cat> catsList = new ArrayList<Cat>();

	@Override
	public List<Cat> findAll() {
		return catsList;
	}

	@Override
	public Cat findById(Long id) {
		if (id < catsList.size()) {
			return catsList.get(Integer.valueOf(id.intValue()));
		} else {
			return null;
		}
	}

	@Override
	public void save(Cat cat) {
		catsList.add(cat);
	}

	@Override
	public void delete(Cat cat) {
		catsList.remove(cat);
	}

	@Override
	public Long count() {
		return new Long(catsList.size());
	}

}
