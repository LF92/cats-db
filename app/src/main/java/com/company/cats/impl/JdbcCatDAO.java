package com.company.cats.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.company.cats.CatDaoRepository;
import com.company.cats.domain.Cat;
import com.company.cats.util.Query;

@Component
@Qualifier("JdbcCatDAO")
public class JdbcCatDAO implements CatDaoRepository {

	@Autowired
	private DataSource dataSource = null;
	private Connection connection = null;
	private PreparedStatement statement = null;
	private ResultSet resultSet = null;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public List<Cat> findAll() {
		System.out.println("JdbcCatDAO");
		List<Cat> catsList = new ArrayList<>();
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(Query.SELECT_ALL_QUERY);
			resultSet = statement.executeQuery();
/*
			while (resultSet.next()) {
				catsList.add(
						new Cat(resultSet.getLong("id"), resultSet.getString("name"), resultSet.getDate("birth_date"),
								resultSet.getFloat("weight"), resultSet.getString("owner_name")));
			}
			*/
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection();
		}
		return catsList;
	}

	@Override
	public Cat findById(Long id) {
		Cat cat = null;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(Query.selectByQuery("id"));
			statement.setLong(1, id);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				cat = new Cat();
				cat.setId(resultSet.getLong("id"));
				cat.setName(resultSet.getString("name"));
				cat.setBirthDate(resultSet.getDate("birth_date"));
				cat.setWeight(resultSet.getFloat("weight"));
				cat.setOwnerName(resultSet.getString("owner_name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection();
		}
		return cat;
	}

	@Override
	public void save(Cat cat) {
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(Query.INSERT_QUERY);
			statement.setString(1, cat.getName());
			statement.setDate(2, new java.sql.Date(cat.getBirthDate().getTime()));
			statement.setFloat(3, cat.getWeight());
			statement.setString(4, cat.getOwnerName());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}

	@Override
	public void delete(Cat cat) {
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(Query.deleteByQuery("id"));
			statement.setLong(1, cat.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection();
		}
	}

	@Override
	public Long count() {
		Long count = 0L;

		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(Query.SELECT_COUNT_QUERY);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				count = resultSet.getLong(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			closeConnection();
		}
		return count;
	}

	private void closeConnection() {
		if (connection != null)
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		if (statement != null)
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		if (resultSet != null)
			try {
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}

}
