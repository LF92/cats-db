package com.company.cats.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.company.cats.CatDaoRepository;
import com.company.cats.domain.Cat;

@Component
@Qualifier("JpaCatDAO")
public class JpaCatDAO implements CatDaoRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Cat> findAll() {
		System.out.println("JpaCatDAO");
		Query query = entityManager.createQuery("SELECT c FROM Cat c");
		List<Cat> list = (List<Cat>) query.getResultList();
		return list;
	}

	@Override
	public Cat findById(Long id) {
		Cat cat = null;
		Query query = entityManager.createQuery("SELECT c FROM Cat c WHERE c.id = :id");
		query.setParameter("id", id);
		try {
			cat = (Cat) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cat;
	}

	@Override
	public void save(Cat cat) {
		entityManager.persist(cat);
	}

	@Override
	public void delete(Cat cat) {
		Query query = entityManager.createQuery("DELETE FROM Cat c WHERE c.id = :id");
		query.setParameter("id", cat.getId());
		query.executeUpdate();
	}

	@Override
	public Long count() {
		Query query = entityManager.createQuery("SELECT count(*) FROM Cat");
		return (Long) query.getSingleResult();
	}
}
