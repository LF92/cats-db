package com.company.cats.util;

public abstract class Query {
	private static final String TABLE_NAME = "cats";

	public static final String INSERT_QUERY = String.format("INSERT INTO %s VALUES (null, ?, ?, ?, ?)", TABLE_NAME);
	public static final String SELECT_ALL_QUERY = String.format("SELECT * FROM %s", TABLE_NAME);
	public static final String SELECT_COUNT_QUERY = String.format("SELECT COUNT(*) FROM %s", TABLE_NAME);

	public static final String deleteByQuery(String arg) {
		return String.format("DELETE FROM %s WHERE %s = ?", TABLE_NAME, arg);
	}

	public static final String selectByQuery(String arg) {
		return String.format("SELECT * FROM %s WHERE %s = ?", TABLE_NAME, arg);
	}
}
