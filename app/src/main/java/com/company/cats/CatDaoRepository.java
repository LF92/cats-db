package com.company.cats;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.company.cats.domain.Cat;

@Repository
@Qualifier("CatDaoRepository")
public interface CatDaoRepository extends org.springframework.data.repository.Repository<Cat, Long> {
	public Cat findById(Long id);

	public <T extends Cat> List<T> findAll();

	public void save(Cat cat);

	public void delete(Cat cat);

	public Long count();
}
