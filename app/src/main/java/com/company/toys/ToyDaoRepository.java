package com.company.toys;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.company.cats.domain.Cat;
import com.company.toys.domain.Toy;

@Repository
@Qualifier("ToyDaoRepository")
public interface ToyDaoRepository extends org.springframework.data.repository.Repository<Toy, Cat> {

	public <T extends Toy> List<T> findByCatOwner(Cat cat);

	public void delete(Toy toy);

	public void save(Toy toy);
	
	public Long count();
}
